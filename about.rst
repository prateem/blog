---
title: About
---
I am Prateem Mandal, Principal Architect at Transunion. My technical interests are in Databases, Query Processing & Optimization, Large Scale Data Processing, Functional Programming, Trusted Computing, Reproducible Builds etc.
