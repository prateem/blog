# Haskell specific packaging instructions taken from
# https://gist.github.com/kuznero/e1f6e53a2ec386a45e579f63b45f53db
# https://nixos.wiki/wiki/Haskell#Overrides
# For mapping between ghc and base versions refer to this table https://www.snoyman.com/base/
{
  inputs = {
    nixpkgs.url = git+https://github.com/nixos/nixpkgs.git?ref=nixpkgs-unstable;
    flake-utils.url = git+https://github.com/numtide/flake-utils.git?ref=main;
  };

  outputs = { self, nixpkgs, flake-utils } @ inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let ghcversion = "ghc947";
          pkgs       = nixpkgs.legacyPackages.${system};
          ghcenv     = nixpkgs.legacyPackages.${system}.haskell.packages.${ghcversion}.ghcWithPackages(ps: with ps; [ cabal-install hakyll pandoc]);
          # ghcenv   = nixpkgs.legacyPackages.${system}.haskellPackages.ghcWithPackages(ps: with ps; [ cabal-install hakyll pandoc]);
      in with pkgs; {
        devShells.default = mkShell {
          packages = [ ghcenv ];
          shellHook = ''
            git status
          '';
        };
      });
}
